/*
    This file is part of The Didactic PDP-8 Assembler
    Copyright (C) 2016 Mark J. Blair <nf6x@nf6x.net>
    Copyright (C) 2002 Toby Thain, toby@telegraphics.com.au

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by  
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License  
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "rb.h"
#include "apl_header.h"

#define HALT_FLAG 0x8000

/* Maximum block length in output file */
#define MAX_BLOCK_LEN 16

/* Leader length in words */
#define LEADER_LEN 8

void readsym(FILE *infile, int verbose_flag) {
    RB_WORD w[3];
    char sym[6];

    if(rb_getwords(infile,w,3)){
	if (verbose_flag) {
	    from_radix50(sym,w[0],w[1]);
	    printf("  \"%s\" = %#o (%s)\n",sym,w[2],rb_sym_types[w[1] & 037]);
	}
    }
}


void no_output(int verbose_flag) {
    if (verbose_flag) {
	printf("  Ignoring this block.\n");
    }
}

void yes_output(int verbose_flag) {
    if (verbose_flag) {
	printf("  Generating object output for this block.\n");
    }
}

int needs_reloc(int rf) {
    if (rf != ABSOLUTE) {
	fprintf(stderr, "ERROR: Non-absolute address found.\n");
	return 1;
    }
    return (0);
}

void write_word(FILE *outfile, uint16_t outword) {
    fputc((outword & 0xff), outfile);
    fputc((outword >> 8), outfile);
}

int convrb(FILE *infile, FILE *outfile, int run_flag, int verbose_flag) {
    RB_WORD	w, header[RB_HEADER_WORDS];
    int		c, i, rf;
    uint16_t	outword[MAX_BLOCK_LEN+1];
    uint16_t	checksum;

    /* skip zero bytes at beginning of file */
    while( !(c = fgetc(infile)) )
	;

    if (c != EOF) {
	ungetc(c,infile);
	rb_checksum = 0;
	while (rb_getwords(infile,header,RB_HEADER_WORDS)) {
	    int t = header[RB_TYPE],
		n = 0200000 - header[RB_WORDCOUNT];
	    if (!t) {
		/* block type 0 (must be time to stop) */
		break;
	    }
	    if (verbose_flag) {
		printf("=%02o= %s Block, %d words, checksum=%06o\n",
		       t, t>=2 && t<=020 ? rb_block_types[t] : "unknown", n, header[RB_CHECKSUM] );
	    }

	    switch (t) {
	      case TITL_BLK:
		no_output(verbose_flag);
		readsym(infile, verbose_flag);
		break;

	      case GADD_BLK:
	      case GREF_BLK:
		no_output(verbose_flag);
		if (verbose_flag) {
		    printf("  address = %06o\n", rb_getword(infile));
		}
		readsym(infile, verbose_flag); 
		rf = getrelflag(header,0);
		if (verbose_flag) {
		    printf("  relflag=%o (%s)\n  expression value = %#o\n",
			   rf,rb_relflag_short[rf], rb_getword(infile));
		}
		break;

	      case COMM_BLK: 
		no_output(verbose_flag);
		readsym(infile, verbose_flag); 
		rf = getrelflag(header,0);
		if (verbose_flag) {
		    printf("  relflag=%o (%s)\n  expression value = %#o\n",
			   rf,rb_relflag_short[rf], rb_getword(infile));
		}
		break;

	      case EXTD_BLK:
	      case ENT_BLK:
	      case GLOC_START_BLK:
	      case EXTN_BLK:
	      case LOCAL_SYM_BLK:
		no_output(verbose_flag);
		for( i = 0 ; i < n/3 && !feof(infile) ; ++i ){
		    rf = getrelflag(header,i);
		    if (verbose_flag) {
			printf("  [%2d] relflag=%o (%s)", i,rf,rb_relflag_short[rf]);
		    }
		    readsym(infile, verbose_flag);
		}
		break;

	      case GLOC_END_BLK:	
	      case LIB_START_BLK:
	      case LIB_END_BLK:
		no_output(verbose_flag);
		break;

	      case REL_DATA_BLK:
		yes_output(verbose_flag);
		if (n < 2) {
		    /* Length needs to be at least 2: address plus
		       at least one data word. */
		    fprintf(stderr, "Invalid block length %d < 2.\n", n);
		    return(EXIT_FAILURE);
		}
		if (n > (MAX_BLOCK_LEN + 1)) {
		    /* Add one to maximum output block length to account for
		       address, which is counted in input count but not in
		       output count. */
		    fprintf(stderr, "Invalid block length %d > %d.\n", n, MAX_BLOCK_LEN + 1);
		    return(EXIT_FAILURE);
		}
		/* First the word count, which does not include the address. */
		outword[0] = ((uint16_t)(n-1) ^ 0xFFFF) + 1;
		checksum = outword[0];
		write_word(outfile, outword[0]);
		for( i = 0 ; i < n && !feof(infile) ; ++i ){
		    rf = getrelflag(header,i);
		    if (needs_reloc(rf)) {
			return (EXIT_FAILURE);
		    }
		    if (verbose_flag) {
			printf("  [%2d] relflag=%o (%s)", i,rf,rb_relflag_short[rf]);
		    }
		    w = rb_getword(infile);
		    if (verbose_flag) {
			printf("  %c %06o\n", i ? ' ' : '@', w);
		    }
		    /* Need to accumulate output words and write them out
		       later, because checksum comes before data in output
		       file. Sigh. */
		    outword[i] = (uint16_t)w;
		    checksum += outword[i];
		}
		/* Write address */
		write_word(outfile, outword[0]);
		/* Write checksum */
		write_word(outfile, (checksum ^ 0xFFFF) + 1);
		/* write data */
		for( i = 1 ; i < n && !feof(infile) ; ++i ){
		    write_word(outfile, outword[i]);
		}
		break;

	      case CSIZ_BLK:
		no_output(verbose_flag);
		for( i = 0 ; i < n && !feof(infile) ; ++i ){
		    rf = getrelflag(header,i);
		    if (verbose_flag) {
			printf("  [%2d] relflag=%o (%s)", i,rf,rb_relflag_short[rf]);
		    }
		    w = rb_getword(infile);
		    if (verbose_flag) {
			printf("  %c %06o\n", i ? ' ' : '@', w);
		    }
		}
		break;

	      case START_BLK:
		yes_output(verbose_flag);
		if (n != 1) {
		    fprintf(stderr, "Expected START block with length of 1, but got %d.\n", n);
		    return(EXIT_FAILURE);
		}
		checksum = 000001;
		write_word(outfile, 000001);
		
		for( i = 0 ; i < n && !feof(infile) ; ++i ){
		    rf = getrelflag(header,i);
		    if (needs_reloc(rf)) {
			return (EXIT_FAILURE);
		    }
		    if (verbose_flag) {
			printf("  [%2d] relflag=%o (%s)", i,rf,rb_relflag_short[rf]);
		    }
		    w = rb_getword(infile);
		    if (verbose_flag) {
			printf("  %c %06o\n", i ? ' ' : '@', w);
		    }
		    outword[i] = (uint16_t)w;
		    if (!run_flag) {
			outword[i] |= HALT_FLAG;
			if (verbose_flag) {
			    printf("  Setting halt flag.\n");
			}
		    }
		    checksum += outword[0];
		    write_word(outfile, outword[0]);
		}
		write_word(outfile, (checksum ^ 0xFFFF) + 1);
		break;

	      default:
		fprintf(stderr, "Unknown block type\n");
		for( i = 0 ; i < n && !feof(infile) ; ++i )
		    fprintf(stderr, "  [%2d] %06o\n",i,rb_getword(infile));
		return(EXIT_FAILURE);
		break;
	    }

	    if(rb_checksum & RB_WORDMASK) {
		fprintf(stderr, "Checksum error (=%06o)\n", rb_checksum & RB_WORDMASK);
		return (EXIT_FAILURE);
	    }

	    if (verbose_flag) {
		printf("\n");
	    }
	}
    }

    return EXIT_SUCCESS;
}

void usage(void) {
    fprintf(stderr, "rb2ab: Convert Nova Relocatable Binary to Absolute Binary.\n");
    fprintf(stderr, "  Usage:\n");
    fprintf(stderr, "    rb2ab [-h] [-a] [-r] <infile.rb> <outfile.ab>\n\n");
    fprintf(stderr, "      -h: Print help and exit.\n");
    fprintf(stderr, "      -a: Prepend auto program load bootstrap.\n");
    fprintf(stderr, "      -r: Tell binary loader to run program after loading;\n");
    fprintf(stderr, "          Otherwise, set flag telling binary loader to halt after loading.\n");
    fprintf(stderr, "\n  Only .rb input files that do not require any relocation are supported.\n");
}


int main(int argc, char **argv) {
    int		flag;
    int		apl_flag = 0;
    int		run_flag = 0;
    int		verbose_flag = 0;
    int		n;
    int		rtn;
    FILE	*infile = NULL;
    FILE	*outfile = NULL;
    
    
    while ((flag = getopt(argc, argv, "ahrv")) != -1) {
	switch(flag) {
	  case 'a':
	    apl_flag = 1;
	    break;

	  case 'h':
	    usage();
	    exit(EXIT_SUCCESS);
	    break;

	  case 'r':
	    run_flag = 1;
	    break;

	  case 'v':
	    verbose_flag = 1;
	    break;

	  case '?':
	  default:
	    fprintf(stderr, "Illegal option flag.\n\n");
	    usage();
	    exit(EXIT_FAILURE);
	    break;
	}
    }

    argc -= optind;
    argv += optind;
    
    if (argc != 2) {
	    fprintf(stderr, "Wrong number of file arguments.\n\n");
	    usage();
	    exit(EXIT_FAILURE);
    }

    infile = fopen(argv[0], "rb");
    if (infile == NULL) {
	fprintf(stderr, "Could not open input file.\n");
	exit(EXIT_FAILURE);
    }

    outfile = fopen(argv[1], "wb");
    if (outfile == NULL) {
	fclose(outfile);
	fprintf(stderr, "Could not open output file.\n");
	exit(EXIT_FAILURE);
    }

    /* Prepend the Auto Program Load header if requested */
    if (apl_flag) {
	for (n=0; n<APL_HEADER_LEN; n++) {
	    fputc(apl_header[n], outfile);
	}
    }

    /* Prepend a leader */
    for (n=0; n<LEADER_LEN; n++) {
	write_word(outfile, 0);
    }
    
    rtn = convrb(infile, outfile, run_flag, verbose_flag);
    
    /* Add a traler */
    for (n=0; n<LEADER_LEN; n++) {
	write_word(outfile, 0);
    }
    
    fclose(outfile);
    fclose(infile);

    return rtn;
}

